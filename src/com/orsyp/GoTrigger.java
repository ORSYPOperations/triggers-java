package com.orsyp;

import java.io.IOException;

public class GoTrigger {

	
	public static void main(String[] args) throws IOException{
		
		TriggerLib trig = new TriggerLib();
		String  authToken = trig.getAuthToken("bsalinux06",6001,"admin","universe");
		TriggerResult result = trig.startTrigger("bsalinux06",6001, authToken, "TRIGGEREXAMPLE");
		trig.deleteAuthToken("bsalinux06",6001, authToken);
	}
}

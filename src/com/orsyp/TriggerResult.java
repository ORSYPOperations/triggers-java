package com.orsyp;

import org.json.JSONException;
import org.json.JSONObject;

public class TriggerResult {

	
	private String TriggerName;
	private String LaunchNumber;
	private String Status;
	
	public TriggerResult(String response){
		 try {
				JSONObject j0 = new JSONObject(response.toString());
				this.Status = j0.getString("status");
				if(Status.equalsIgnoreCase("success")){
					JSONObject j1 = new JSONObject(j0.get("data").toString().replace("[","").replace("]", ""));
					this.TriggerName = j1.get("trigger").toString();
					this.LaunchNumber = j1.get("launch_number").toString();
				}else{
					this.TriggerName="";
					this.LaunchNumber="";
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
	}
	
	public String getTriggerName(){
		return this.TriggerName;
	}
	public String getLaunchNumber(){
		return this.LaunchNumber;
	}
	public String getStatus(){
		return this.Status;
	}
	
}

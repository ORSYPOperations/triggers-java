package com.orsyp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;

public class TriggerLib {

	public String deleteAuthToken(String Hostname, int Port, String AuthToken) throws IOException{
		
		String url = "http://"+Hostname+":"+Integer.toString(Port)+"/access?token="+AuthToken;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("DELETE");
		int responseCode = con.getResponseCode();
		if(responseCode != 200){
			return "Error! Request Could Not Be Processed. Error code returned: "+responseCode;
		}
		return Integer.toString(responseCode);
	}
	public TriggerResult startTrigger(String Hostname, int Port, String AuthToken, String EventTypeName) throws IOException{
		
		String url = "http://"+Hostname+":"+Integer.toString(Port)+"/event";
		
		String urlParameters = "type="+EventTypeName;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		
		con.setRequestMethod("POST");
		
		con.setRequestProperty("Authorization", AuthToken);
		 con.setUseCaches (false);
	      con.setDoInput(true);
	      con.setDoOutput(true);

		  DataOutputStream wr = new DataOutputStream (
				  con.getOutputStream ());
      wr.writeBytes(urlParameters);
      wr.flush ();
      wr.close ();
      InputStream is = con.getInputStream();
      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
      String line;
      StringBuffer response = new StringBuffer(); 
      while((line = rd.readLine()) != null) {
        response.append(line);
        //response.append('\r');
      }
      rd.close();
      
      TriggerResult res = new TriggerResult(response.toString());
  
      return res;

	}
	public String getAuthToken(String Hostname, int Port, String User, String Password) throws IOException{
		
		String url = "http://"+Hostname+":"+Integer.toString(Port)+"/access";
		 
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod("GET");
 
		//add request header
		con.addRequestProperty("user", User);
		con.addRequestProperty("pwd", Password);
 
		int responseCode = con.getResponseCode();

		String AuthToken ="";
		if(responseCode != 200){
			return "Error! Request Could Not Be Processed. Error code returned: "+responseCode;
		}
		
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		try {
			JSONObject j0 = new JSONObject(response.toString());
			JSONObject j1 = new JSONObject(j0.get("data").toString().replace("[","").replace("]", ""));
			AuthToken = j1.get("token").toString();

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return AuthToken;
	}
		
}
